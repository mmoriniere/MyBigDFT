.. MyBigDFT documentation master file, created by
   sphinx-quickstart on Wed Jul 11 16:21:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MyBigDFT's documentation!
====================================

.. mdinclude:: ../../README.md

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code_doc
   tutorials
   exercises


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
